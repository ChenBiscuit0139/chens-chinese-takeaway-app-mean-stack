import { Item } from './item';

export class Order {
    items: Item[];
    total: number;
}
