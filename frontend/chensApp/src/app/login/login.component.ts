import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { ApiService } from '../api.service';

import { FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginUser = this.fb.group({
    username: ['', Validators.required],
    password: ['', Validators.required]
  });

  get username() { return this.loginUser.get('username'); }
  get password() { return this.loginUser.get('password'); }

  constructor(
    private router: Router,
    private apiService: ApiService,
    private fb: FormBuilder) { }

  ngOnInit() {
  }

  onLogin(form) {
    this.apiService.login(form.value).subscribe(res => {
      if (res.status) {
        this.apiService.setLoggedInStatus(true);
        this.router.navigate(['/home']);
      } else {
        form.form.controls.password.setErrors({ invalid: true });
      }
    }, (err) => {
      console.log(err);
    });
  }

  clear(form) {
    form.form.controls.password.setErrors(null);
  }
}
