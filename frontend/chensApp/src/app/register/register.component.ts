import { Component, OnInit } from '@angular/core';
import {
  FormControl,
  Validators,
  FormBuilder,
  FormGroup,
  FormGroupDirective,
  NgForm,
  AbstractControl,
  AsyncValidatorFn
} from '@angular/forms';
import { Router } from '@angular/router';
import { ErrorStateMatcher } from '@angular/material';

import { ApiService } from '../api.service';

import { Register } from '../class/register';
import { map, catchError } from 'rxjs/operators';
import { Observable } from 'rxjs';


export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const invalidCtrl = !!(control && control.invalid && control.parent.dirty);
    const invalidParent = !!(control && control.parent && control.parent.invalid && control.parent.dirty);

    return (invalidCtrl || invalidParent);
  }
}

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  error = '';
  register: Register = { username: null, password: null, confirmPassword: null };

  registerForm = this.fb.group({
    username: ['', Validators.required, this.validateUsername()],
    password: ['', Validators.required],
    confirmPassword: ['', Validators.required]
  }, { validator: this.checkPasswords });

  matcher = new MyErrorStateMatcher();

  constructor(
    private router: Router,
    private apiService: ApiService,
    private fb: FormBuilder) { }

  ngOnInit() {
  }

  get username() { return this.registerForm.get('username'); }
  get password() { return this.registerForm.get('password'); }
  get confirmPassword() { return this.registerForm.get('confirmPassword'); }

  checkPasswords(group: FormGroup) {
    const pass = group.controls.password.value;
    const confirmPass = group.controls.confirmPassword.value;

    return pass === confirmPass ? null : { notSame: true };
  }

  onRegister(form) {
    this.apiService.register(form.value).subscribe(res => {
      if (res.status) {
        this.router.navigate(['/login']);
      }
    }, (err) => {
      console.log(err);
    });
  }

  validateUsername(): AsyncValidatorFn {
    return (control: AbstractControl): Observable<{ [key: string]: any }> => {
      return this.apiService.checkUsername(control.value)
        .pipe(
          map(res => {
            if (res.status) {
              return { usernameExists: true };
            }
          })
        );
    };
  }
}
