<?php
// Include config file
require_once "config.php";

// Get the posted data.
$postdata = file_get_contents("php://input");


if(isset($postdata) && !empty($postdata))
{
    // Extract the data.
    $request = json_decode($postdata);

    // Validate.
    if(empty(trim($request->username)))
    {
        return http_response_code(400);
    }

    // Sanitize.
    $username = mysqli_real_escape_string($connect, trim($request->username));
    
    // Prepare a select statement
    $sql = "SELECT id FROM users WHERE username = ?";

    if($stmt = mysqli_prepare($connect, $sql)){
        // Bind parameter
        mysqli_stmt_bind_param($stmt, "s", $param_username);

        // // Set parameter
        $param_username = mysqli_real_escape_string($connect, trim($request->username));

        // Attempt to execute the prepared statement
        if(mysqli_stmt_execute($stmt)){

            /* store result */
            mysqli_stmt_store_result($stmt);

            // Check for matching username
            if(mysqli_stmt_num_rows($stmt) == 1){
                $username_exists = true;
            }
            else{
                $username_exists = false;
            }
        }
    }
    // Close statement
    mysqli_stmt_close($stmt);

    // Send response
    http_response_code(201);
    $res = [
        'data' => $username_exists
    ];
    echo json_encode($res);
}
?>