<?php

header("Access-Control-Allow-Credentials: true");
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Methods: PUT, GET, POST, DELETE");
header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
session_start();

// error_log($_SESSION['user']);

$login = [
    'loggedIn' => true,
    'message' => $_SESSION['user'],
];
echo $login;

// if(isset($_SESSION['user'])) {
//     echo '{"loggedIn": true}';
// }
// else {
//     echo '{"loggedIn": false}';
// }
