<?php
// Include config file
require_once "config.php";

// Get the posted data.
$postdata = file_get_contents("php://input");


if(isset($postdata) && !empty($postdata))
{
    // Extract the data.
    $request = json_decode($postdata);

    // Validate.
    if(empty(trim($request->username)) || empty(trim($request->password1)) || empty(trim($request->password2)))
    {
        return http_response_code(400);
    }

    // Sanitize.
    $username = mysqli_real_escape_string($connect, trim($request->username));
    $password = mysqli_real_escape_string($connect, trim($request->password1));
    $confirm_password = mysqli_real_escape_string($connect, trim($request->password2));
    
    if($password == $confirm_password){
        // Prepare an insert statement
        $sql = "INSERT INTO users (username, password) VALUES (?, ?)";
     
        if($stmt = mysqli_prepare($connect, $sql)){
            // Bind parameters
            mysqli_stmt_bind_param($stmt, "ss", $param_username, $param_password);
            
            // Set parameters
            $param_username = $username;
            $param_password = password_hash($password, PASSWORD_DEFAULT); // Creates a password hash
            
            // Attempt to execute the prepared statement
            if(mysqli_stmt_execute($stmt)){
                $register = true;
            }
            else{
                http_response_code(401);
            }
        }
        // Close statement
        mysqli_stmt_close($stmt);

    }
    else {
        $register = false;
    }

    // Send response
    http_response_code(201);
    $res = [
        'registered' => $register
    ];
    echo json_encode($res);

    // Close connection
    mysqli_close($connect);
}
?>