<?php
session_start();
require_once 'config.php';

// Get the posted data.
$postdata = file_get_contents("php://input");

if(isset($postdata) && !empty($postdata))
{
    // Extract the data.
    $request = json_decode($postdata);

    // Validate.
    if(empty(trim($request->username)) || empty(trim($request->password)))
    {
        return http_response_code(400);
    }

    // Sanitize.
    $username = mysqli_real_escape_string($connect, trim($request->username));
    $password = mysqli_real_escape_string($connect, trim($request->password));

    // Prepare a select statement
    $sql = "SELECT id, username, password FROM users WHERE username = ?";

    // Validate credentials
    if($stmt = mysqli_prepare($connect, $sql)){
        // Bind parameter
        mysqli_stmt_bind_param($stmt, "s", $param_username);
        
        // Set parameter
        $param_username = $username;
        
        // Attempt to execute the prepared statement
        if(mysqli_stmt_execute($stmt)){
            // Store result
            mysqli_stmt_store_result($stmt);
            
            // Check if username exists
            if(mysqli_stmt_num_rows($stmt) == 1){  
                
                // Bind result variables
                mysqli_stmt_bind_result($stmt, $id, $username, $hashed_password);
                if(mysqli_stmt_fetch($stmt)){
                    // Verify password
                    if(password_verify($password, $hashed_password)){
                        // Password is correct
                        $loggedIn = true;
                        $_SESSION['user'] = $username;
                    }
                    else{
                        // Incorrect password
                        $loggedIn = false;
                    }
                }
            }
            else{
                // Username doesn't exist
                $loggedIn = false;
            }
        }
        else{
            // Failed to execute statement
        }
    }
    
    // Close statement
    mysqli_stmt_close($stmt);

    // Send response
    http_response_code(201);
    $login = [
        'loggedIn' => $loggedIn,
    ];

    echo json_encode($login);
}

// Close connection
mysqli_close($connect);
?>