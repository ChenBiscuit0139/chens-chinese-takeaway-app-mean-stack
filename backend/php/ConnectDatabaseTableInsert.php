<?php
// Define Variables
define('DB_HOST', 'localhost');
define('DB_USER', 'root');
define('DB_PASS', '');
define('DB_NAME', 'mydb');

// Connect
$connect = mysqli_connect(DB_HOST, DB_USER, DB_PASS);

// Check connection
if (mysqli_connect_errno($connect)) {
    die("Error: Failed to connect." . mysqli_connect_error());
}

// Print host information
echo "Connect Successfully. Host info: " . mysqli_get_host_info($connect) . "<br>";



// Create Database
$sql = "CREATE DATABASE " . DB_NAME;
if(mysqli_query($connect, $sql)){
    echo "Database created successfully" . "<br>";
}
else{
    echo "ERROR: Could not able to execute $sql. " . mysqli_error($connect) . "<br>";
}

// Close connection
mysqli_close($connect);

// New connection
$connect = mysqli_connect(DB_HOST, DB_USER, DB_PASS, DB_NAME);

// Check new connection
if (mysqli_connect_errno($connect)) {
    die("Error: Failed to connect." . mysqli_connect_error());
}

// Create Table
$sql = "CREATE TABLE users(
    id INT NOT NULL PRIMARY KEY AUTO_INCREMENT, 
    username varchar(50) NOT NULL, 
    password varchar(50) NOT NULL
)";
if(mysqli_query($connect, $sql)){
    echo "Table created successfully" . "<br>";
}
else{
    echo "ERROR: Could not able to execute $sql. " . mysqli_error($connect) . "<br>";
}

// Insert 
$sql = "INSERT INTO users (id, username, password) VALUES (NULL, ?, ?)";
if($stmt = mysqli_prepare($connect, $sql)){
    mysqli_stmt_bind_param($stmt, "ss", $parameter_username, $parameter_password);
    $parameter_username = "username1";
    $parameter_password = "password1";
    mysqli_stmt_execute($stmt);

    $parameter_username = "username2";
    $parameter_password = "password2";
    mysqli_stmt_execute($stmt);

    echo "Record inserted successfully" . "<br>";
}
else{
    echo "ERROR: Could not able to execute $sql. " . mysqli_error($connect) . "<br>";
}

// Close connection
mysqli_close($connect);
?>