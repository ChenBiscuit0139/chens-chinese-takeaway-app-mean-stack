CREATE TABLE IF NOT EXISTS items (
    id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(50) NOT NULL UNIQUE,
    category VARCHAR(50),
    description VARCHAR(255),
    price DECIMAL(5,2),
    rating DOUBLE CHECK (rating>=0 AND rating<=5)
);

INSERT INTO items (id, name, category, description, price, rating) VALUES
(1, 'item1', 'category1', 'description1', 1.00, 5.0 ),
(2, 'item2', 'category1', 'description2', 2.00, 4.5 ),
(3, 'item3', 'category2', 'description3', 3.00, 4.0 ),
(4, 'item4', 'category2', 'description4', 4.00, 3.5 ),
(5, 'item5', 'category3', 'description5', 5.00, 3.0 ),
(6, 'item6', 'category3', 'description6', 6.00, 2.5 ),
(7, 'item7', 'category4', 'description7', 7.00, 2.0 ),
(8, 'item8', 'category4', 'description8', 8.00, 1.5 ),
(9, 'item9', 'category5', 'description9', 9.00, 1.0 ),
(10, 'item10', 'category6', 'description10', 10.00, 0.5 );
