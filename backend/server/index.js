const mongoose = require('mongoose')
const bodyParser = require('body-parser')
const app = require('express')()

const url = 'mongodb://mongo:27017/chensDB'

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: false }))

app.use(function (req, res, next) {
    res.header('Access-Control-Allow-Origin', '*')
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')
    res.header('Access-Control-Allow-Methods', 'PUT, POST, GET, DELETE, OPTIONS')
    return next()
})

// Schemas

// User
const userSchema = new mongoose.Schema({
    username: { type: String, required: true, unique: true },
    password: { type: String, required: true }
}, { collection: 'users' })

const User = mongoose.model('User', userSchema)

// Username
const usernameSchema = new mongoose.Schema({
    username: { type: String, required: true, unique: true }
}, { collection: 'users' })

const Username = mongoose.model('Username', usernameSchema)

let itemSchema = new mongoose.Schema({
    name: String,
    description: String,
    price: Number
}, { collection: 'items' })

itemSchema.statics.initItem = Item => {

    let _items = [];
    for (let i = 0; i <= 10; i++) {
        _items.push({
            name: 'item' + i.toString(),
            description: 'description' + i.toString(),
            price: 0.99 + i
        })
    }

    Item.remove({}, (err) => {
        if (err) console.error(err)
        _items.forEach(item => {
            Item.create(item)
        })
    })
}

const Item = mongoose.model('Item', itemSchema)

// Connect to mongoDB

mongoose
    .connect(
        url,
        { useNewUrlParser: true },
        (err) => {
            if (err) throw err
            Item.initItem(Item)
        }
    )
    .then(() => console.log('MongoDB Connected'))
    .catch(err => console.error(err))

// Get Items

app.get('/getItems', (req, res) => {
    getItems(res);
})

// Login

app.post('/login', (req, res) => {
    if (req.body.username && req.body.password) {
        findUser(req, res);
    } else {
        res.status(404).end();
    }
})

// Register

app.post('/register', (req, res) => {
    if ((req.body.username) && (req.body.password === req.body.confirmPassword)) {
        saveUser(req, res);
    } else {
        res.status(404).end();
    }
})

// Validate username

app.post('/validateUsername', (req, res) => {
    if (req.body.username) {
        validateUsername(req, res);
    } else {
        res.status(404).end();
    }
})

function findUser(req, res) {
    User.find({
        username: req.body.username,
        password: req.body.password
    }, (err, user) => {
        if (err) return console.error('Find user error: ', err)
        userStatus(res, user);
    })
}

function userStatus(res, user) {
    if (user.length === 1) {
        res.status(200).json({
            status: true
        })
    } else {
        res.status(200).json({
            status: false
        })
    }
}

function saveUser(req, res) {
    const user = new User({
        username: req.body.username,
        password: req.body.password
    })
    user.save().then(
        res.status(200).json({
            status: true
        })
    )
}

function validateUsername(req, res) {
    Username.find({
        username: req.body.username
    }, (err, username) => {
        if (err) return console.error('Validate username error: ', err)
        usernameStatus(username, res);
    })
}

function usernameStatus(username, res) {
    if (username.length === 1) {
        res.status(200).json({
            status: true
        })
    } else {
        res.status(200).json({
            status: false
        })
    }
}

function getItems(res) {
    Item.find({}, (err, items) => {
        if (err) return console.error('Get items error: ', err)
        res.status(200).json(items)
    })
}

app.listen(3000, () => console.log('Server running on port 3000'))
